import React, { useState } from "react";
import "./tables.css";
import Gosc from "./Gosc";
import uuid from "uuid";
import Stolik from "./Stolik";


function Stoliki() {
    const [guest, setGuest] = useState("");
    const [guestlist, setGuestlist] = useState([]);
    const [table, setTable] = useState([]);

    const handleChange = (event) => {
        const { value } = event.target
        setGuest(value)
    }
    const handleClick = () => {
        setGuestlist([...guestlist, { value: guest, key: uuid() }])
        setGuest("")
    }

    const handleDelete = (item) => () => {
        setGuestlist([...guestlist.filter((r) => r !== item)])
    }

    const handleAdd = (item) => () => {
        setTable([...table, {value: item, key: uuid()}])
    }

    return (
        <div>
            <h1 className="title">Wesele Kulów</h1>
            <input
                placeholder="Wpisz imię i nazwisko gościa"
                value={guest}
                onChange={handleChange} />
            <button
                onClick={handleClick}
                disabled={guest === ""}>Dodaj do listy</button>
            <div className="lista">
                <ol>
                    {guestlist.map(guest => <Gosc
                        key={guest.key}
                        guest={guest.value}
                        onDelete={handleDelete(guest)} 
                        onAdd={handleAdd(guest.value)}/>)}
                </ol>
            </div>
            {table.map(gosc => <Stolik
            gosc={gosc.value}/>)}
        </div>
    )
}

export default Stoliki