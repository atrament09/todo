import React, { useState } from "react";
import AfricaCapital from "./AfricaData";

function GeoquizAfrica() {
    const [selectValue, setselectvalue] = useState("")
    const [country, setCountry] = useState("")


    const handleChange = (event) => {
        const { value } = event.target
        setselectvalue(value)
    }

    const handleClick = () => {
        setCountry(AfricaCapital.find(
            (stolica) => stolica.country === selectValue))
        }

    return (
        <div>
            <h3>Wybierz kraj:</h3>
            <select
                value={selectValue}
                onChange={handleChange}>
                {AfricaCapital.map((kraj) => <option>
                    {kraj.country}</option>)}
            </select>
            <button onClick={handleClick}> Sprawdź
                </button>
            <p>{country.capital}</p>
        </div>
    )
}

export default GeoquizAfrica 