import React, {useState} from 'react';
import './quiz-rome.css';

function Zabytki(props) {
    const monuments = [
        { name: "Koloseum", foto: "./assets/Koloseum.jpg", value: "koloseum", id: "1" },
        { name: "Fontanna Di Trevi", value: "fontana", id: "2", foto: "./assets/Fontana-di-Trevi.jpg" },
        { name: "Forum Romanum", value: "forum", id: "3", foto: "./assets/Forum-Romanum.jpg" },
        { name: "Spanish Steps", value: "spanishSteps", id: "4", foto: "./assets/Spanish-Steps.jpg" },
    ]
const [romanum, setRomanum] = useState(monuments)

const wybory = (event) => {
    const value = event.target.value;
    console.log({value})
}

    return (
        <div className="oneCard">
            <img src={props.foto} alt="foto" />
            <h3> {props.id}. {props.name} </h3>
            <select
            onChange={wybory}> 
            {monuments.map(v => <option value={v.value} key={v.id}>{v.name}</option>)}
            </select>
            <button> sprawdz </button>
            <p>ODPOWIEDŹ</p>
        </div>
    )
}

export default Zabytki 