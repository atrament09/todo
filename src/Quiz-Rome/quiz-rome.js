import React from 'react';
import "./quiz-rome.css";
import Zabytki from './onezab.js'

function QuizRome(props) {
    return (
        <div>
            <Zabytki name="Koloseum" foto="./assets/Koloseum.jpg" value="koloseum" id= "1" />
            <Zabytki name="Fontanna Di Trevi" value="fontana" id="2" foto="./assets/Fontana-di-Trevi.jpg"/>
            <Zabytki name="Forum Romanum" value="forum" id="3" foto="./assets/Forum-Romanum.jpg"/>
            <Zabytki name="Spanish Steps" value="spanishSteps" id="4" foto="./assets/Spanish-Steps.jpg"/>
        </div>
    )}

    export default QuizRome