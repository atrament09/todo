import React from "react";
import cls from "classnames";

function Movie({movie, onDone: handleDone, onRemove: handleRemove, isDone}) {
const cssMovie = cls({"done": isDone});

    return (
        <div>
            <li className={cssMovie}>{movie.value}</li>
            <button onClick={handleDone}>Obejrzane</button>
            <button onClick={handleRemove}>Usuń</button>
        </div>
    )
}

export default Movie