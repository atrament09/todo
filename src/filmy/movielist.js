import React, { useState } from "react";
import Movie from "./movie";
import "./movielist.css";
import uuid from "uuid/v4";

function Movieslist() {
    const [inputValue, setInputValue] = useState("");
    const [movielist, setMovielist] = useState([])

    const handleOnChange = (event) => {
        const { value } = event.target
        setInputValue(value)
    }

    const handleButtonClick = () => {
        setMovielist([...movielist, { value: inputValue, id: uuid() }])
        setInputValue("");
    }
    const handleDone = (item) => () => {
        setMovielist(movielist.map(d => {
            if (d === item) {
                d.done = true
            } return d
        }))
    }
    const handleRemove = (item) => () => {
        setMovielist([...movielist.filter((p) => p !== item)]);
    }

    return (
        <div>
            <h2 className="threeD">Lista filmów do obejrzenia</h2>
            <input
                placeholder="Wpisz tytuł filmu"
                onChange={handleOnChange}
                value={inputValue}
            />
            <button
                onClick={handleButtonClick}
                disabled={inputValue === ""}>
                Dodaj</button>

            {movielist.map((movie, idx) => (
                <Movie
                    key={movie.id}
                    movie={movie}
                    onDone={handleDone(movie)}
                    isDone={movie.done}
                    onRemove={handleRemove(movie)}
                />
            ))}
        </div>
    )
}

export default Movieslist 