import React, { useState } from 'react';
import Przyjaciele from './friendsDataBase';
import './quiz.css'

function Quiz() {
    const pytanie = "Kim jest osoba na zdjęciu ?"
    const [friends, setFriends] = useState(Przyjaciele)
    const [wybrany, setWybrany] = useState("")

    const klikniete = (friend) => (event) => {
        const { value } = event.target
        console.log(value)
        // setWybrany(value);
        setFriends(friends.map(v => {
            if (v.value === friend.value) {
                v.selectedValue = value;
            } return v;
        }))
    }
    const przycisk = (value) => {
        console.log('ok')
        setWybrany(() => 'OK')
    }

    const wraperfunction = (friend) => (event) => {
        if (friend.selectedValue === friend.value) {
            console.log('ok')
        }
        event.preventDefault();
    }

    return (
        <div className="quizOne">
            {friends.map(oneQuiz =>
                <div key={oneQuiz.id} className="quest">
                    <form>
                        <h2> Pyt.{oneQuiz.id} </h2>
                        {pytanie}
                        <br />
                        <br />
                        <img src={oneQuiz.foto} />
                        <br />
                        <br />
                        <select
                            value={oneQuiz.selectedValue}
                            onChange={klikniete(oneQuiz)}>
                            <option value=""> Wybierz z listy </option>
                            {friends.map(friend => <option key={friend.value} value={friend.value}> {friend.name} </option>)}
                        </select>
                        <button
                            disabled={oneQuiz.selectedValue === ""}
                            onClick={wraperfunction(oneQuiz)}
                            value={friends.map(oneQuiz => oneQuiz.value)}> sprawdz </button>
                        <p> {oneQuiz.selectedValue}</p>
                    </form>
                </div>)}
        </div>
    )
}

export default Quiz