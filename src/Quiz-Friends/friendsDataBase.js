const Przyjaciele = [
    { name: "Rachel", surname: "Green", foto: "/assets/RachelGreen.jpg", age: 27, dateOfBirth: 1980 - 5 - 23, id:"1", value:"rachel"},
    { name: "Ross", surname: "Geller", foto: "/assets/RossGeller.jpg", age: 29, dateOfBirth: 1978 - 11 - 15, id:"2", value:"ross" },
    { name: "Monica", surname: "Geller", foto: "/assets/MonicaGeller.jpg", age: 27, dateOfBirth: 1980 - 7 - 15, id:"3",value:"monica"},
    { name: "Chandler", surname: "Bing", foto: "/assets/ChandlerBing.jpg", age: 29, dateOfBirth: 1978 - 2 - 13, id:"4", value:"chandler" },
    { name: "Phoebe", surname: "Buffay", foto: "/assets/PhoebeBuffay.jpg", age: 30, dateOfBirth: 1977 - 9 - 4, id:"5", value:"phoebe" },
    { name: "Joey", surname: "Tribiani", foto: "/assets/JoeyTribbiani.jpg", age: 28, dateOfBirth: 1979 - 3 - 26, id:"6", value:"joey" }
];

export default Przyjaciele