import React from "react";
import cls from "classnames";

function Song({ isDone, song, onRemove: handleRemove, onDone: handleDone }) {
    const cssSong = cls({ 'done': isDone });

    return (
        <div>
            <li className={cssSong}>{song.value}</li>
            <button
                onClick={handleRemove}> Done </button>
            <button
                onClick={handleDone}> Usuń </button>
        </div>
    )
}

export default Song