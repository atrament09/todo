import React, { useState } from "react";
import Song from "./song";
import "./songlist.css";
import uuid from "uuid/v4";

function Songlist() {
    const [inputValue, setinputValue] = useState("");
    const [songlist, setSonglist] = useState([]);

    const handleOnChange = (event) => {
        const { value } = event.target
        setinputValue(value)
    }

    const handleClick = () => {
        setSonglist([...songlist, { value: inputValue, id: uuid() }])
        setinputValue("");
    }

    const handleRemove = (item) => () => {
        setSonglist([...songlist.filter((p) => p !== item)]);
    }

    const handleDone = (item) => () => {
        setSonglist(songlist.map(d => {
            if (d === item) {
                d.done = true
            } return d
        }))
    }
    return (
        <div>
            <h2 className="threeD">Mój songlist</h2>
            <input
                className="input"
                placeholder="Wpisz nazwę piosenki"
                value={inputValue}
                onChange={handleOnChange} />

            <button
                onClick={handleClick}
                disabled={inputValue === ""}
            > Dodaj </button>

            {songlist.map((song, idx) => (
                <Song
                    key={song.id}
                    song={song}
                    onRemove={handleRemove(song)}
                    onDone={handleDone(song)}
                    isDone={song.done}
                />
            ))}
        </div>
    )
}

export default Songlist