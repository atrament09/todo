import React from 'react';
import cls from 'classnames';

function Todo({ isDone, todo, onDelete: handleDelete, onDone: handleDone }) {
    console.log({ isDone, todo, onDelete: handleDelete, onDone: handleDone });
    const cssTodo = cls({ 'done': isDone });
    return (
        <div className="todo">
            <li className={cssTodo}> {todo.value} </li>
            <button
                type="button"
                onClick={handleDone} > Zrobione </button>
            <button
                type="button"
                onClick={handleDelete}> Usuń </button>
        </div>
    )
}

export default Todo

