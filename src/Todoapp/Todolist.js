import React, { useState, useEffect } from "react";
import Todo from "./todo";
import './todo.css';
import axios from 'axios';

const ENDPOINT = 'http://localhost:3005/todos';
function TodoList(props) {
    const [todoList, setTodoList] = useState([]);
    const [inputValue, setinputValue] = useState("");

    const handleInputChange = (event) => {
        const { value } = event.target
        setinputValue(value)
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        handleButtonClick();
    }

    const handleButtonClick = () => {
        const newItem = { value: inputValue, id: btoa((new Date()).getTime()) };
        console.error({ newItem });
        axios.post(ENDPOINT, newItem)
            .then((response) => {
                setTodoList([...todoList, newItem]);
                setinputValue("");
            })
            .catch((error) => {
                alert('ERROR!');
                console.error(error);
            })

    }

    const handleDelete = (item) => () => {
        axios.delete(`${ENDPOINT}/${item.id}`)
            .then((response) => {
                setTodoList([...todoList.filter((p) => p !== item)])
            })
            .catch((error) => {
                alert('ERROR!');
                console.error(error);
            })
    }

    const handleDone = (item) => () => {
        axios.patch(`${ENDPOINT}/${item.id}`, { done: !item.done })
            .then((response) => {
                setTodoList(todoList.map(d => {
                    if (d === item) {
                        d.done = !d.done
                    } return d
                }
                ))
            })
            .catch((error) => {
                alert('ERROR!');
                console.error(error);
            })

    }
    useEffect(() => {
        if (!isUpdated) {
            axios.get(ENDPOINT)
                .then((response) => {
                    setIsUpdated(true);
                    setTodoList(response.data)
                    console.log(response.data)
                })
                .catch(console.error)
        }
    })
    const [isUpdated, setIsUpdated] = useState(false)

    return (
        <div className="todolist">
            <form
                onSubmit={handleSubmit}>
                <input
                    name="todo input"
                    placeholder="Co będziemy dzisiaj robić?"
                    value={inputValue}
                    onChange={handleInputChange} />
                <button
                    type="button"
                    onClick={handleButtonClick}
                    disabled={inputValue === ""}
                >Dodaj zadanie do listy</button>
            </form>

            {todoList.map((todo, idx) => (
                <Todo
                    key={todo.id}
                    todo={todo}
                    onDelete={handleDelete(todo)}
                    onDone={handleDone(todo)}
                    isDone={todo.done}
                />
            ))}
        </div>
    )
}

export default TodoList

