import React, { useState, useEffect } from "react";
import Stolice from "./geobase";

function Geoquiz(props) {
    const [selectValue, setSelectValue] = useState("")
    const [kraj, setKraj] = useState("")

    useEffect(() => {
        console.log(selectValue);
    }, [selectValue])

    const handelOnchange = (event) => {
        const { value } = event.target
        setSelectValue(value)
    }
    const handelOnClick = () => {
        setKraj(Stolice.find(
            (stolica) => stolica.country === selectValue))
        }

    return (
        <div>
            <h3>Wybierz kraj:</h3>
            <select
                value={selectValue}
                onChange={handelOnchange}>
                <option value=""> Wybierz kraj:</option>
                {Stolice.map((kraj => <option>
                    {kraj.country}</option>))}
            </select>
            <button onClick={handelOnClick} 
            disabled={selectValue === ""}>
                 Sprawdź nazwę stolicy</button>
            <p>{kraj.capital}</p>

        </div>
    )
}

export default Geoquiz